var express    = require("express"),
   mysql       = require('mysql'),
   app         = express(),
   bodyParser  = require("body-parser"),
   methodOver  = require("method-override"),
   dateformat  = require("dateformat");

   app.use(express.static(__dirname + '/public'));
   app.use(bodyParser.urlencoded({extended: true}));
   app.use(bodyParser.json());

   app.set("view engine","ejs");
   app.use(methodOver("_method"));

    // =================
    // Credential for DB
    // =================

      var con = mysql.createConnection({
         host: "localhost",
         user: "raviy",
         password: "ztlab341",
         database:"mydb"
      });

                        //=====================
                        // Connection To MySQL
                        //=====================

      con.connect(function(err) {
            if (err) throw err;
             console.log("Connected ! to MySQL Successfully");
      });

      //==============
      //EMPLOYEE CRUD
      //==============

      app.get("/employee",function(req,res){
            con.query("SELECT * FROM Employee",function(error,result,fields) {
            if (error){
            throw error;
            }else{
            res.status(200).send(result);
            }
         });
      });

      app.post("/employee/add",function(req,res){
         const { name, emp_no,dept_id,join_date,end_date} = req.body;
            con.query("INSERT INTO Employee SET?",{
               name: name,
               emp_no: emp_no,
               dept_id: dept_id,
               join_date: join_date,
               end_date: end_date
             }, (error,results) => {
            if(error){
               throw error;
            }else{
               res.status(200).send(results);
            }
         });
      });

      app.put("/employee/edit/:id",function(req,res){
         var query="UPDATE `Employee` SET `name`='"+req.body.name+"',`emp_no`='"+req.body.emp_no+"',`join_date`='"+req.body.join_date+"',`end_date`='"+req.body.end_date+"',`dept_id`='"+req.body.dept_id+"' WHERE `id`="+req.params.id+"";
         con.query(query, function(error,result){
            if(result){
               res.status(200).send(result);
            }else{
               res.status(400);
            }
         });
      });

      app.delete("/employee/delete/:id", (req,res) => {
            const id = req.params.id;
            con.query("DELETE FROM Employee WHERE id = "+id, (error,result) => {
               if(error){
               throw error;
               }else{
               res.status(200).send(result);
            }
         });
      });

      //==============
      //SALARY CRUD
      //==============

      app.get("/salary",function(req,res){
            con.query("SELECT * FROM Salary",(error,result) => {
               if (error){
               throw error;
               }else{
               res.status(200).send(result);
            }
         });
      }); 

      app.post("/salary/add",function(req,res){
         const { emp_id, month, year, amount, generated_date } = req.body;
            con.query("INSERT INTO Salary SET?",{
               emp_id: emp_id,
               month: month,
               year: year,
               amount: amount,
               generated_date: generated_date
             }, (error,results) => {
            if(error){
               throw error;
            }else{
               res.status(200).send(results);
            }
         });
      });

      app.put("/salary/edit/:id",function(req,res){
         var query="UPDATE `Salary` SET `emp_id`='"+req.body.emp_id+"',`month`='"+req.body.month+"',`year`='"+req.body.year+"',`amount`='"+req.body.amount+"',`generated_date`='"+req.body.generated_date+"' WHERE `id`="+req.params.id+"";
         con.query(query, function(error,result){
            if(result){
               res.status(200).send(result);
            }else{
               res.status(400);
            }
         });
      });

      app.delete("/salary/delete/:id", (req,res) => {
         const id = req.params.id;
         con.query("DELETE FROM Salary WHERE id = "+id, (error,result) => {
            if(error){
            throw error;
            }else{
            res.status(200).send(result);
         }
      });
   });
   
      //===============
      //DEPARTMENT CRUD
      //=============== 

      app.get("/department",function(req,res){
         con.query("SELECT * FROM Department",function(error,result){
            if (error){
            throw error;
            }else{
            res.status(200).send(result);
         }
        });
      });

      app.post("/department/add",function(req,res){
         const { name,created_date } = req.body;
         con.query("INSERT INTO Department SET?",{
            name: name,
            created_date: created_date
          }, (error,results) => {
            if(error){
               throw error;
            }else{
               res.status(200).send(results);
            }
         });
      });

      app.put("/department/edit/:id",function(req,res){
         var query="UPDATE `Department` SET `name` ='"+req.body.name+"' WHERE `id`='"+req.params.id+"'";
         con.query(query, function(error,result){
            if(result){
               res.status(200).send(result);
            }else{
               res.status(400);
            }
         });
      });

      app.delete("/department/delete/:id", (req,res) => {
         const id = req.params.id;
         con.query("DELETE FROM Department WHERE id = "+id, (error,result) => {
            if(error){
            throw error;
            }else{
            res.status(200).send(result);
         }
      });
   });

                        //==================
                        //SERVER LISTEN PORT
                        //==================

      let port= 8082;
         app.listen (port,function(){
         console.log("Server is started Listening on Port",port);
      });
